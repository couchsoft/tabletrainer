#!/bin/bash

cd client
ng build
cd ../api
mvn clean install
cd ..
java -jar api/target/tabletrainer-1.0.0-SNAPSHOT.jar server api/config.yml
