package de.couchsoft.tabletrainer;

import io.dropwizard.Configuration;

public class TabletrainerConfiguration extends Configuration {

	private Integer competitionExerciseCount;

	private String databaseFileLocation;

	public Integer getCompetitionExerciseCount() {
		return competitionExerciseCount;
	}

	public void setCompetitionExerciseCount(Integer competitionExerciseCount) {
		this.competitionExerciseCount = competitionExerciseCount;
	}

	public String getDatabaseFileLocation() {
		return databaseFileLocation;
	}

	public void setDatabaseFileLocation(String databaseFileLocation) {
		this.databaseFileLocation = databaseFileLocation;
	}

}
