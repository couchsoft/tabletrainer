package de.couchsoft.tabletrainer.core;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.dropwizard.lifecycle.Managed;

public class DatabaseManager implements Managed {

	private final Logger LOG = LoggerFactory.getLogger(DatabaseManager.class);

	private final String fileLocation;

	public EntityManagerFactory emf;

	public DatabaseManager(String fileLocation) {
		this.fileLocation = fileLocation;
	}

	@Override
	public void start() throws Exception {
		LOG.debug("Creating EntityManagerFactory");
		emf = Persistence.createEntityManagerFactory(fileLocation + "/tabletrainer.odb");
		LOG.debug("EntityManagerFactory is open? " + emf.isOpen());
	}

	@Override
	public void stop() throws Exception {
		LOG.debug("Closing EntityManagerFactory");
		emf.close();
		LOG.debug("EntityManagerFactory is open? " + emf.isOpen());
	}

}
