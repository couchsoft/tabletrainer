package de.couchsoft.tabletrainer;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.couchsoft.tabletrainer.core.DatabaseManager;
import de.couchsoft.tabletrainer.health.DummyHealthCheck;
import de.couchsoft.tabletrainer.resources.CompetitionResource;
import de.couchsoft.tabletrainer.resources.PracticeResource;
import de.couchsoft.tabletrainer.resources.ResultResource;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

public class TabletrainerApplication extends Application<TabletrainerConfiguration> {

	private final Logger LOG = LoggerFactory.getLogger(TabletrainerApplication.class);

	public static void main(final String[] args) throws Exception {
		new TabletrainerApplication().run(args);
	}

	@Override
	public String getName() {
		return "tabletrainer";
	}

	@Override
	public void initialize(final Bootstrap<TabletrainerConfiguration> bootstrap) {
		bootstrap.addBundle(new AssetsBundle("/assets/", "/", "index.html"));
	}

	@Override
	public void run(final TabletrainerConfiguration configuration, final Environment environment) {
		// managed objects
		DatabaseManager db = new DatabaseManager(configuration.getDatabaseFileLocation());
		environment.lifecycle().manage(db);

		// we use HK2 to inject db connection into resources
		environment.jersey().register(new AbstractBinder() {
			@Override
			protected void configure() {
				bind(db).to(DatabaseManager.class);
			}
		});

		// all the healthchecks
		final DummyHealthCheck healthCheck = new DummyHealthCheck();
		environment.healthChecks().register("dummy", healthCheck);

		// register the resources
		environment.jersey().register(new PracticeResource());
		environment.jersey().register(new CompetitionResource(configuration.getCompetitionExerciseCount()));
		environment.jersey().register(new ResultResource());
	}

}
