package de.couchsoft.tabletrainer.health;

import com.codahale.metrics.health.HealthCheck;

public class DummyHealthCheck extends HealthCheck {

	public DummyHealthCheck() {
	}

	@Override
	protected Result check() throws Exception {
		// we have no config, no database, so nothing should go wrong
		return Result.healthy();
	}
}
