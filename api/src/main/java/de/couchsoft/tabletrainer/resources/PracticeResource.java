package de.couchsoft.tabletrainer.resources;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.codahale.metrics.annotation.Timed;

import de.couchsoft.tabletrainer.api.Exercise;

@Path("/practice")
@Produces(MediaType.APPLICATION_JSON)
public class PracticeResource {

	public PracticeResource() {
	}

	@GET
	@Timed
	public Exercise get(@QueryParam("multipliers") List<Integer> multipliers) {
		return new Exercise(multipliers);
	}

	@POST
	@Timed
	public Exercise validate(@NotNull @Valid Exercise exercise) {
		exercise.validate();
		return exercise;
	}

}
