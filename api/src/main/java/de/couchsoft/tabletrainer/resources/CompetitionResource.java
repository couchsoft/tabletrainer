package de.couchsoft.tabletrainer.resources;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.codahale.metrics.annotation.Timed;

import de.couchsoft.tabletrainer.api.CompetitionExercise;

@Path("/competition")
@Produces(MediaType.APPLICATION_JSON)
public class CompetitionResource {

	private final int competitionExerciseCount;

	public CompetitionResource(int competitionExerciseCount) {
		this.competitionExerciseCount = competitionExerciseCount;
	}

	@GET
	@Timed
	public CompetitionExercise get() {
		return new CompetitionExercise(competitionExerciseCount);
	}

	@POST
	@Timed
	public CompetitionExercise validate(@NotNull @Valid CompetitionExercise exercise) {
		exercise.validate();
		return exercise;
	}
}
