package de.couchsoft.tabletrainer.resources;

import javax.ws.rs.QueryParam;
import javax.persistence.TypedQuery;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.ValidationException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.codahale.metrics.annotation.Timed;

import de.couchsoft.tabletrainer.api.CompetitionResult;
import de.couchsoft.tabletrainer.core.DatabaseManager;

@Path("/result")
@Produces(MediaType.APPLICATION_JSON)
public class ResultResource {

	private final Logger LOG = LoggerFactory.getLogger(ResultResource.class);

	@Inject
	private DatabaseManager db;

	public ResultResource() {
	}

	@POST
	@Timed
	public void persist(@NotNull @Valid CompetitionResult result) throws ValidationException {
		if (!result.success())
			// TODO: This should throw 400 but throws 500 error
			throw new ValidationException("You cannot store unsolved competitions");
		EntityManager em = db.emf.createEntityManager();
		em.getTransaction().begin();
		em.persist(result);
		em.getTransaction().commit();
		LOG.info("Created new CompetitionResult");
	}

	@GET
	@Timed
	public List<CompetitionResult> get() {
		EntityManager em = db.emf.createEntityManager();
		List<CompetitionResult> results = em
				.createQuery("SELECT r FROM CompetitionResult r ORDER BY r.exercise.duration ASC", CompetitionResult.class)
				.setMaxResults(10).getResultList();
		return results;
	}

	@GET
	@Path("/position")
	@Timed
	public Integer position(@NotNull @QueryParam("millis") Integer millis) {
		EntityManager em = db.emf.createEntityManager();
		TypedQuery<CompetitionResult> query = em.createQuery(
				"SELECT r FROM CompetitionResult r WHERE r.exercise.duration < :duration", CompetitionResult.class);
		Integer result = query.setParameter("duration", millis).getResultList().size();
		return result;
	}

}
