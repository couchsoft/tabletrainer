package de.couchsoft.tabletrainer.api;

import java.util.Random;
import java.util.List;
import java.util.ArrayList;
import javax.persistence.Embeddable;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

@Embeddable
public class Exercise {

	@JsonProperty
	@NotNull
	@Min(value = 1)
	@Max(value = 10)
	Integer multiplier;

	@JsonProperty
	@NotNull
	@Min(value = 1)
	@Max(value = 10)
	Integer multiplicand;

	@JsonProperty
	@Min(value = 1)
	@Max(value = 100)
	Integer result;

	@Min(value = 1)
	@Max(value = 100)
	@JsonProperty
	Integer expectedResult;

	@JsonProperty
	Boolean success = false;

	// TODO: Add a duration

	public Exercise() {
		this(new ArrayList<Integer>());
	}

	public Exercise(List<Integer> multipliers) {
		Random r = new Random();
		multiplier = r.nextInt(10) + 1;
		if (!multipliers.isEmpty()) {
			while (true) {
				Integer trial = r.nextInt(10) + 1;
				if (multipliers.contains(trial)) {
					multiplier = trial;
					break;
				}
			}
		}
		multiplicand = r.nextInt(10) + 1;
		expectedResult = multiplier * multiplicand;
	}

	public void validate() {
		success = (result == expectedResult);
	}

}
