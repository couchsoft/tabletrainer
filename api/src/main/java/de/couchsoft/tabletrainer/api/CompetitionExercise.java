package de.couchsoft.tabletrainer.api;

import java.util.Calendar;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Embeddable;

import com.fasterxml.jackson.annotation.JsonProperty;

@Embeddable
public class CompetitionExercise {

	@JsonProperty
	List<Exercise> exercises = new ArrayList<>();

	@JsonProperty
	Timestamp begin = new Timestamp(Calendar.getInstance().getTime().getTime());

	@JsonProperty
	Timestamp end;

	@JsonProperty
	Integer failureCount;

	@JsonProperty
	Long duration;

	public CompetitionExercise() {

	}

	public CompetitionExercise(int exerciseCount) {
		failureCount = exerciseCount;
		for (int i = 0; i < exerciseCount; i++) {
			exercises.add(new Exercise());
		}
	}

	public void validate() {
		end = new Timestamp(Calendar.getInstance().getTime().getTime());
		duration = (end.getTime() - begin.getTime());
		for (Exercise ex : exercises) {
			ex.validate();
			if (ex.success)
				failureCount--;
		}
	}

}
