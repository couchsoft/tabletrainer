package de.couchsoft.tabletrainer.api;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class CompetitionResult {

	@Id
	@GeneratedValue
	Long id;

	@JsonProperty
	@NotNull
	CompetitionExercise exercise;

	@JsonProperty
	@NotEmpty
	String name;

	@JsonProperty
	@NotEmpty
	String school;

	@JsonProperty
	@NotNull
	Integer age;

	public Boolean success() {
		return (exercise.failureCount == 0);
	}

}
