FROM openjdk:8

WORKDIR /var/www/tabletrainer

COPY api/config.yml .
COPY api/target/tabletrainer-1.0.0-SNAPSHOT.jar .

# used by objectdb as logdir as objectdb can't log to apllication log
RUN mkdir log
RUN chown www-data:www-data log

# here the database is created and /data is mapped to a volume
RUN mkdir /data
RUN ln -s /data data
RUN chown www-data:www-data /data

EXPOSE 8080

USER www-data:www-data

CMD [ "java", "-jar", "tabletrainer-1.0.0-SNAPSHOT.jar", "server", "config.yml" ]
