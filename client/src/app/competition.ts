import { Exercise } from './exercise';

export class Competition {
  exercises: Array<Exercise>;
  begin: Date;
  end: Date;
  duration: number;
  failureCount: number;
};
