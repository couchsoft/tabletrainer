import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { Competition } from '../competition';
import { Result } from '../result';
import { CompetitionService } from '../competition.service';
import { ResultService } from '../result.service';

@Component({
  selector: 'app-competition',
  templateUrl: './competition.component.html',
  styleUrls: ['./competition.component.css']
})
export class CompetitionComponent implements OnInit {

  competition: Competition = new Competition();
  position: number;
  result: Result = new Result();
  validation: boolean;

  constructor(private competitionService: CompetitionService, private resultService: ResultService, private router: Router) { }

  ngOnInit() {
    this.generate();
  }

  generate(): void {
    this.validation = false;
    this.competitionService.generate().subscribe(competition => {
      this.competition = competition;
    });
  }

  validate(): void {
    this.validation = true;
    this.competitionService.validate(this.competition).subscribe(competition => {
      this.competition = competition;
      if(competition.failureCount == 0) {
        this.result.exercise = competition;
        this.resultService.position(this.competition.duration).subscribe(position => this.position = position + 1);
      }
    });
  }

  save(): void {
    this.resultService.save(this.result).subscribe(result => {
      this.router.navigate(['/result']);
    });
  }

}
