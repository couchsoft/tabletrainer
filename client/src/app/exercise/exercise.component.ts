import { Component, OnInit, EventEmitter } from '@angular/core';

import { Exercise } from '../exercise';
import { PracticeService } from '../practice.service';

@Component({
    selector: 'app-exercise',
    templateUrl: './exercise.component.html',
    styleUrls: ['./exercise.component.css']
})

export class ExerciseComponent implements OnInit {

    exercise: Exercise = new Exercise();
    validation: boolean;
    focusInput: EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor(private practiceService: PracticeService) { }

    ngOnInit() {
      this.generate();
    }

    generate(): void {
      this.validation = false;
      this.practiceService.generate().subscribe(exercise => {
        this.exercise = exercise;
        this.focusInput.emit(true);
      });
    }

    validate(): void {
      this.practiceService.validate(this.exercise).subscribe(exercise => {
        this.validation = true;
        this.exercise = exercise;
      });
      setTimeout(() => {
        this.generate();
      }, 2000);
    }

}
