import { Competition } from './competition';

export class Result {
  exercise: Competition;
  name: string;
	school: string;
  age: number;
}
