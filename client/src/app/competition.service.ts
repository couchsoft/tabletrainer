import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Competition } from './competition';

@Injectable({
  providedIn: 'root'
})
export class CompetitionService {

  constructor(private http: HttpClient) { }

  generate(): Observable<Competition> {
    return this.http.get<Competition>('/api/competition');
  }

  validate(competition: Competition): Observable<Competition> {
    return this.http.post<Competition>('/api/competition', competition);
  }

}
