import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Result } from './result';

@Injectable({
  providedIn: 'root'
})
export class ResultService {

  constructor(private http: HttpClient) { }

  list(): Observable<Result[]> {
    return this.http.get<Result[]>('/api/result');
  }

  save(result: Result): Observable<Result> {
    return this.http.post<Result>('/api/result', result);
  }

  position(millis: number): Observable<number> {
    return this.http.get<number>(`/api/result/position?millis=${millis}`);
  }

}
