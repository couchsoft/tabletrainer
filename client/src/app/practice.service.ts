import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Exercise } from './exercise';

@Injectable({
  providedIn: 'root'
})
export class PracticeService {

  constructor(private http: HttpClient) { }

  generate(): Observable<Exercise> {
    return this.http.get<Exercise>('/api/practice');
  }

  validate(exercise: Exercise): Observable<Exercise> {
    return this.http.post<Exercise>('/api/practice', exercise);
  }

}
