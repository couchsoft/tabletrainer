import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ExerciseComponent } from './exercise/exercise.component';
import { CompetitionComponent } from './competition/competition.component';
import { ResultComponent } from './result/result.component';

const routes: Routes = [
  { path: '', redirectTo: '/practice', pathMatch: 'full' },
  { path: 'practice', component: ExerciseComponent },
  { path: 'competition', component: CompetitionComponent },
  { path: 'result', component: ResultComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, {useHash: true}) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule { }
