import { Component, OnInit } from '@angular/core';

import { Result } from '../result';
import { ResultService } from '../result.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  results: Result[];

  constructor(private resultService: ResultService) { }

  ngOnInit() {
    this.list();
  }

  list(): void {
    this.resultService.list().subscribe(results => this.results = results);
  }

}
