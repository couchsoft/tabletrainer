export class Exercise {
  multiplier: number;
  multiplicand: number;
  result: number;
  expectedResult: number;
  success: boolean;
}
