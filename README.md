# Tabletrainer

## How to start the Tabletrainer API

1. Run `mvn clean install` to build your application
1. Start application with `java -jar target/tabletrainer-1.0.0-SNAPSHOT.jar server config.yml`
1. To check that your application is running enter url `http://localhost:8080`

## Health Check

To see your applications health enter url `http://localhost:8081/healthcheck`

## Tabletrainer UI
Use `ng serve` during development of the UI. To access enter url `http://localhost:4200`. 

## Build it all together
```
cd tabletrainer-client
ng build 
cd ../tabletrainer-api
mvn clean install
java -jar target/tabletrainer-1.0.0-SNAPSHOT.jar server config.yml
```

`http://localhost:8080`
